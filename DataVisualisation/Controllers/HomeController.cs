﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace DataVisualisation.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult GetAllStops()
        {
            var encoding = Encoding.GetEncoding(1250);
            string json = "";
            using (StreamReader r = new StreamReader(Server.MapPath("~/data.json"), encoding))
            {
                json = r.ReadToEnd();
            }
            var l = JsonConvert.DeserializeObject<List<Stop>>(json);
            return Json(l, JsonRequestBehavior.AllowGet);
        }
    }

    public class Stop
    {
        [JsonProperty("lat")]
        public float Lat { get; set; }

        [JsonProperty("lon")]
        public float Lon { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }


        public override string ToString()
        {
            return Name + ": " + Lon + ", " + Lat;

        }
    }
}